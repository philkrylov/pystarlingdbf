from setuptools import setup

setup(
    name='pystarlingdbf',
    version='0.1',
    py_modules=['pystarlingdbf'],
    url='',
    license='MIT',
    author='Phil Krylov',
    author_email='phil.krylov@gmail.com',
    description='StarLing DBF file reader',
    install_requires=['pystarlingencoding'],
)
