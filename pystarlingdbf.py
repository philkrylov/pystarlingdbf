import codecs
import datetime
import decimal
from pathlib import PurePath
import struct
from typing import Generator, List

import pystarlingencoding


codecs.register(lambda enc: pystarlingencoding.getregentry() if enc == 'starling' else None)
stardecode = pystarlingencoding.getregentry().decode
decode = lambda v: stardecode(v)[0]


def dbf_reader(path: PurePath,
               skip_deleted: bool = True) -> Generator[List, None, None]:
    """Returns an iterator over records in a StarLing DBF file.

    The first row returned contains the field specs and record count.
    The subsequent rows contain the data records.

    File should be opened for binary reads.
    """
    # See DBF format spec at:
    #     http://www.pgts.com.au/download/public/xbase.htm#DBF_STRUCT

    with open(str(path), 'rb') as f:
        numrec, lenheader = struct.unpack('<xxxxLH22x', f.read(32))
        numfields = (lenheader - 33) // 32
        var_file, old_var = None, False

        try:
            fields = []
            for fieldno in range(numfields):
                name, typ, typ2, size, deci = struct.unpack('<11scc3xBB14x', f.read(32))
                name, typ, typ2 = name.decode('ascii'), typ.decode('ascii'), typ2.decode('ascii')
                if typ + typ2 == 'CV':
                    typ = 'V'
                    if not var_file:
                        var_file = open(str(path.with_suffix('.var')), 'rb')
                        old_var = var_file.read(1) != b'\x13'
                name = name.replace('\0', '')  # eliminate NULs from string
                if typ == 'C' and deci:
                    size += deci << 8
                    deci = 0
                fields.append((name, typ, size, deci))
            yield [fields, numrec]

            fields.insert(0, ('DeletionFlag', 'C', 1, 0))
            fmt = ''.join(['%ds' % fieldinfo[2] for fieldinfo in fields])
            fmtsiz = struct.calcsize(fmt)
            f.seek(lenheader)
            for i in range(numrec):
                record = struct.unpack(fmt, f.read(fmtsiz))
                if skip_deleted and record[0] != b' ':
                    continue  # deleted record
                result = []
                for (name, typ, size, deci), value in zip(fields, record):
                    if name == 'DeletionFlag':
                        continue
                    if typ == "N":
                        value = value.replace(b'\0', b'').lstrip()
                        if value == b'':
                            value = 0
                        elif deci:
                            value = decimal.Decimal(value)
                        else:
                            value = int(value)
                    elif typ == 'D':
                        y, m, d = int(value[:4]), int(value[4:6]), int(value[6:8])
                        value = datetime.date(y, m, d)
                    elif typ == 'L':
                        value = (value in b'YyTt' and 'T') or (value in b'NnFf' and 'F') or '?'
                    elif typ == 'V':
                        offset, length = struct.unpack('<LH', value)
                        if offset in (0x20202020, 0):
                            value = ''
                        else:
                            assert var_file, ".var file could not be opened"
                            var_file.seek(offset)
                            if old_var and length == 0:
                                length, = struct.unpack('<L', var_file.read(4))
                            value = decode(var_file.read(length))
                    elif typ == 'C':
                        value = decode(value)
                    result.append(value)
                yield result
        finally:
            if var_file:
                var_file.close()
