# pystarlingdbf

[StarLing](https://starlingdb.org/downl.php?lan=en#soft) DBF/VAR file reader.

## Installation

    pip install git+https://gitlab.com/philkrylov/pystarlingencoding.git
    pip install git+https://gitlab.com/philkrylov/pystarlingdbf.git

## Usage

    def dbf_reader(path: PurePath,
                  skip_deleted: bool = True) -> Generator[List, None, None]:

Returns an iterator over records in a StarLing DBF file.

The first row returned contains the field specs `[(name, type, length, decimals), ...]` and record count.
The subsequent rows contain the data records.

## License

MIT.
